package logger

/* simple package to write and manage text logs */

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"time"
)

func WriteLog(logName string, message string) {
	var file *os.File

	//check if a log exists already. create a new one if not
	file, err := os.OpenFile(logName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	if err != nil {
		file, err = os.Create(logName)
		if err != nil {
			panic(err)
		}
	}

	//make a formatted, date stamped string
	t := time.Now()
	timeStr := t.Format("01-02-2006 15:04:05")
	newEntry := fmt.Sprintf("%s - %s\r\n", timeStr, message)

	_, err = file.WriteString(newEntry)
	if err != nil {
		panic(err)
	}

	file.Close()
}

//this is setup for windows line endings, make a different one for linux, probably
func WriteError(logName string, message string, errorMessage error) {
	var file *os.File

	//open a log for writing. create new if it doesn't exist
	file, err := os.OpenFile(logName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("couldn't open log: ", logName)
	}

	w := bufio.NewWriter(file)

	//make a formatted, date stamped string
	t := time.Now()
	timeStr := t.Format("01-02-2006 15:04:05")

	//create log entry
	var errStr error

	if errorMessage != nil {
		errStr = errorMessage
	} else {
		errStr = errors.New("**no error message**")
	}

	newEntry := fmt.Sprintf("%s - %s: %s \r\n", timeStr, message, errStr)

	_, err = fmt.Fprint(w, newEntry)
	if err != nil {
		fmt.Println("couldn't write log to: ", logName)
	}

	w.Flush()
	file.Close()
}

func TruncateLog(logName string) {
	var file *os.File

	//open a log
	file, err := os.OpenFile(logName, os.O_WRONLY, 0744)
	if err != nil {
		fmt.Printf("couldn't open log: %s - error: %e", logName, err)
	}

	err = file.Truncate(0)
	if err != nil {
		fmt.Println("couldn't truncate log file: ", err)
	}

	file.Seek(0, 0)
	file.Close()
}

func ArchiveLog(logName string) {
	//this function archives a log by appending a datestamp
	//to the filename
	var file *os.File

	//open file in read only mode
	file, err := os.Open(logName)
	if err != nil {
		fmt.Printf("couldn't open log: %s - error: %e", logName, err)
		return
	}

	//suffix datestamp to log name
	t := time.Now()
	yesterday := t.AddDate(0, 0, -1)
	timeStr := yesterday.Format("01-02-2006")

	//remove file extension (assuming .log)
	trimmed := strings.TrimSuffix(logName, ".log")

	archiveName := fmt.Sprintf("%s_%s.log", trimmed, timeStr)

	//open archive to write into
	archiveFile, err := os.OpenFile(archiveName, os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Printf("couldn't open archive log: %s - error: %e", logName, err)
		return
	}

	//copy data
	_, err = io.Copy(archiveFile, file)
	if err != nil {
		fmt.Println("copying error: ", err)
		return
	}

	//close files
	file.Close()
	archiveFile.Close()

	//truncate original log after archive
	TruncateLog(logName)
}
